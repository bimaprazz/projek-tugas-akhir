function validateForm() {
            if (document.forms["formPendaftaran"]["nama"].value == "") {
                alert("Nama Tidak Boleh Kosong");
                document.forms["formPendaftaran"]["nama"].focus();
                return false;
            }
            if (document.forms["formPendaftaran"]["nim"].value == "") {
                alert("Email Tidak Boleh Kosong");
                document.forms["formPendaftaran"]["nim"].focus();
                return false;
            }
            if (document.forms["formPendaftaran"]["jurusan"].value == "") {
                alert("Jurusan Tidak Boleh Kosong");
                document.forms["formPendaftaran"]["jurusan"].focus();
                return false;
            }
            if (document.forms["formPendaftaran"]["hp"].value == "") {
                alert("No. Hp Tidak Boleh Kosong");
                document.forms["formPendaftaran"]["hp"].focus();
                return false;
            }
            if (document.forms["formPendaftaran"]["alamat"].value == "") {
                alert("Alamat Tidak Boleh Kosong");
                document.forms["formPendaftaran"]["alamat"].focus();
                return false;
            }
            if (document.forms["login"]["username"].value == "") {
                alert("Username Tidak Boleh Kosong");
                document.forms["login"]["username"].focus();
                return false;
            }
            if (document.forms["login"]["password"].value == "") {
                alert("Password Tidak Boleh Kosong");
                document.forms["login"]["password"].focus();
                return false;
            }
        }